app.config(function($httpProvider){
   $httpProvider.interceptors.push('myInterceptor');
})
.controller('depositCtr',['$scope','$rootScope','$http','$translate',function($scope,$rootScope,$http,$translate){
	$rootScope.webtitle=$translate.instant('Deposit')
	$rootScope.curPage='deposit';
	 
	 $scope.curAsset={};
	 $scope.curAsset.total="--";
	 $scope.curAsset.free="--";
	 $scope.curAsset.locked="--";
	 $scope.curAsset.chargeAddress='--';
	
	 
	 $scope.layout=function(){
			var win_h=$(window).height();
			var top_h=$('.header').height();
			var bottom_h=$('.footer').height();
			var dw_title_h=$('.dw-title').height();
			var min_h=win_h-top_h-bottom_h-dw_title_h-15;
			var left_h=$('.dw-body>.f-fl').height();
			$('.dw-body>.f-fl,.dw-body>.f-fr').css('min-height',Math.max(min_h,left_h));
			$scope.rows=Math.floor(($('.dw-body>.f-fr').height()-30)/34);
			$scope.getHistory(0,$scope.curPage,$scope.rows);
		}
	 
	 /*获取充值记录*/
		$scope.depositHistory=[];
		$scope.curPage=1;
		$scope.getHistory=function(direction,page,rows){
			 var conf={
					 method:"post",
					 url:"/user/getMoneyLog.html",
					 data:"coin=&status=&direction="+direction+"&page="+page+"&rows="+rows
			 }
			 $http(conf).success(function(data){
				 $scope.depositHistory=data.data;
				 $scope.curPage=data.pages;
				 $scope.total=data.total;
				 $scope.rows=data.rows;
				 $scope.pages=data.pages;
			 })
		 }
	 
	 /*选择资产*/
	 $scope.selectAsset=function(asset){
			 sessionStorage.depositCoin=asset.asset;
			 $scope.curAsset=asset;
			 $scope.curAsset.logoSrc=$scope.assetLogo[asset.asset];
			 $scope.getSymbols(asset.asset);
			 if(!asset.forceStatus){
				 $scope.getChargeAddress(asset.asset,asset.sameAddress,asset.forceStatus);
			 }else{
				 $scope.showAddressTip=true;
			 }
			 $scope.showAssets=false;
			 setTimeout(function(){
				 $scope.layout();
			 },0)
	 }
	
	/*获取持仓*/
	 $scope.getUserAsset=function(asset){
	   var ajaxUrl;
	   if(asset){
		   ajaxUrl='/exchange/private/userAsset?asset='+asset;
	   }else{
		   ajaxUrl='/exchange/private/userAsset';
	   }
	   $http.post(ajaxUrl).success(function(data){
		   if(asset){
			   var curAsset=data[0];
			   curAsset.free=Number(curAsset.free);
			   curAsset.freeze=Number(curAsset.freeze);
			   curAsset.locked=Number(curAsset.locked);
			   curAsset.total=curAsset.free*1+curAsset.locked*1+curAsset.freeze*1+curAsset.withdrawing*1;
			   $scope.selectAsset(curAsset);
		   }else{
			   $scope.userAssets=data;
			   angular.forEach($scope.userAssets,function(value,index){
		            $scope.userAssets[index].free=Number(value.free);
		            $scope.userAssets[index].freeze=Number(value.freeze);
		            $scope.userAssets[index].locked=Number(value.locked);
		            $scope.userAssets[index].total=value.free*1+value.locked*1+value.freeze*1+value.withdrawing*1;
		       })
		   }
		   
	   })
	 }
	 
	 /*获取所有币种是否可以充值提现*/
	 $scope.assetParams={}
	 $scope.getAllAsset=function(){
	       $http.get('/assetWithdraw/getAllAsset.html').success(function(data){
	    	   angular.forEach(data,function(value){
	            	var obj={};
	            	obj.enableCharge=value.enableCharge;
	            	obj.enableWithdraw=value.enableWithdraw;
	            	obj.confirmTimes=value.confirmTimes;
	            	obj.minProductWithdraw=value.minProductWithdraw;
	            	obj.cnLink=value.cnLink;
	            	obj.enLink=value.enLink;
	            	$scope.assetParams[value.assetCode]=obj;
	            })
	       })
	 }
	
	/*获取asset logo*/
	$scope.assetLogo={};
	$scope.getAssetLogo=function(){
		$http.post('/dictionary/getAssetPic.html').success(function(data){
			angular.forEach(data.data,function(value,index){
				$scope.assetLogo[value.asset]=value.pic;
				
			})
		})
	}
	
	 
	
	var coin=sessionStorage.depositCoin;
	if(coin){
		$scope.getUserAsset(coin)
		$scope.getAllAsset();
		$scope.getAssetLogo();
	}else{
		$scope.layout();
	}
	
	 
	
	 
	 /*获取充值地址*/
	 $scope.showAddressTip=true;//是否显示充值地址强提示
	 $scope.getChargeAddress=function(coin,isSameAddress,forceStatus){
			var params;
			if(forceStatus){
				params=$.param({coin:coin,sameAddress:isSameAddress,status:true});
			}else{
				params=$.param({coin:coin,sameAddress:isSameAddress});
			}
			$http.post('/charge/getChargeAddress.html',params).success(function(data){
				$scope.curAsset.chargeAddress=data.address;
				if(!data.address){
					$scope.curAsset.chargeAddress='--';
				}
				$scope.curAsset.chargeAddressTag=data.addressTag;
				if(!data.addressTag){
					$scope.curAsset.chargeAddressTag='--';
				}
				
				setTimeout(function(){
					var clipboard=new Clipboard('.btn-copy');
				    clipboard.on('success', function(e) {
				    	layer.msg($translate.instant("Succeed"),{icon:1,time:1500})
				        
				    });
				    clipboard.on('error', function(e) {
				    	layer.msg($translate.instant("failed"),{icon:1,time:1500})
				    });
				},1000)
				if(forceStatus){
					$scope.showAddressTip=false;
				}
			})
		}
	 
	 	/*获取资产交易市场*/
	 	$scope.getSymbols=function(asset){
	 		$http.get('/asset/getSymbolByAsset.html?asset='+asset).success(function(res){
	 			if(res.success){
	 				$scope.curAsset.symbols=res.symbols;
	 			}
	 		})
	 	}
	 
	    /*生成新充值地址*/
		$scope.generateUserChargeAddress=function(asset){
			var params=$.param({coin:asset.asset,resetAddressStatus:asset.resetAddressStatus})
			$http.post('/charge/resetChargeAddress.html',params).success(function(data){
				if(data.success){
					$scope.curAsset.chargeAddress=data.model.address;
					layer.msg($translate.instant('Succeed'),{icon:1,time:2500});
				}else{
					layer.msg(data.msg,{icon:2,time:2500});
				}
			})
		}
	 
	 
	
	$scope.getAllAssetInfo=function(event){
		$scope.showAssets=!$scope.showAssets;
		if($scope.showAssets){
			$scope.getUserAsset();
			$scope.getAllAsset();
			$scope.getAssetLogo();
			setTimeout(function(){
				$(event.target).siblings('ul').find('input[type=text]').focus();
			},0)
		}
	}
	
	/*生成二维码*/
	$scope.createQrcode=function(asset,isLabel){
		$('#ewm').html('');
		
		if(isLabel){
			$scope.ewmBoxTitle=$translate.instant("Deposits")+asset.assetLabel;
			$scope.ewmText=asset.chargeAddressTag;
		}else{
			$scope.ewmBoxTitle=$translate.instant("Deposit Address");
			$scope.ewmText=asset.chargeAddress;
		}
		jQuery('#ewm').qrcode({
            render    : "canvas",//也可以替换为table
            foreground: "#333",
            background: "#FFF",
            width:200,
            height:200,
            text: $scope.ewmText
        });
		layer.open({
			title:null,
			type:1,
			area:['432px','460px'],
			content:$("#ewmBox")
		})
	}
	
	/*跳转交易*/
	$scope.changeProduct=function(baseAsset,quoteAsset){
    	localStorage.ProStatus = baseAsset+quoteAsset;
    	localStorage.setStatus = baseAsset+"_"+quoteAsset;
    	localStorage.quoteAsset = quoteAsset;
    	window.location.href="/trade.html?symbol="+baseAsset+"_"+quoteAsset;
    }
	
	
	
	$scope.showDetail=function(index){
		var _this=$('.lists-body li').eq(index);
		_this.toggleClass('cur').find('table').slideToggle();
		_this.siblings().removeClass('cur').find('table').hide();
	}
}])

.filter("assetFilter",function(){
    return function(input,num){
       var output;
       if(input=='--'){
    	  output=input;
       }else{
    	  output=input.toFixed(num);
       }
       return output;
    };
})




.filter("parsefloat",function(){
    return function(input){
        var output=parseFloat(input);
          
        return output;
    };
})

.filter('advancefilter', ['$filter', function($filter){
	return function(data, keys,seach){

		var result=[]
		angular.forEach(data, function(test){
			if(seach){
				for(key in keys){
					if(keys[key]){
						if(test[keys[key]].toLowerCase().indexOf(seach.toLowerCase())!=-1){
							if(result.indexOf(test)==-1)
							result.push(test)
						}
					}
				}

			}else{
				result.push(test)
			}
		});

		return result;
	}
}]);

